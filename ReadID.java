import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * реализовано: удаление лишних пробелов
 * удаление описания к программе
 * требуется доделать: удаление строчных коментариев
 *
 * программа компелируется в  записывающем файле
 * Created by karab on31.05.2017.
 *
 * @Карабанов 15оит18
 */

public class ReadID {
    public static void main(String[] args) throws IOException {
        /**
         *@Param bufferedWriter запись данных в файла.
         */
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\manyFiles.java"));
        String s;
        /**
         *@Param pattern регулярка на считывание оставшихся знаков(букв,чисел,знаков понктуации)после pattern1,2
         *@Param pattern1 ругуляррка на исключение описания
         *@Param pattern2 пока не работает в перспективе должен убирать строчные коменттарии
         */
        Pattern pattern = Pattern.compile("\\w*[\\w,\\d,_]*[^{};(),+[]][]],]*[^:]*\\W*\\D");
        Pattern pattern1 = Pattern.compile("\\b*[\\w\\d]*[*//*//][ ][A-z0-9]*");
        Pattern pattern2 = Pattern.compile("//?\\w+");
        Matcher matcher = pattern.matcher("*");
        Matcher matcher1 = pattern1.matcher("*");
        Matcher matcher2 = pattern2.matcher("*");
        /**
         *@Param bufferedReader считывает данные из файла
         */
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("c:\\work\\manyFles\\src\\manyFiles.java"))) {
            while ((s = bufferedReader.readLine()) != null) {
                /**
                 *удаляет лишние пробелы
                 */
                while (s.contains("  ")) {
                    String replace = s.replace("  ", " ");
                    s = replace;
                }
                matcher.reset(s);
                matcher1.reset(s);
                while (!matcher1.find() && !matcher2.find() && matcher.find()) {
                    /**
                     * записывает и выводит на экран результаты проверки
                     */
                    bufferedWriter.write(matcher.group()+"\n");
                    System.out.println(matcher.group());
                }
            }

        }
        bufferedWriter.close();
    }
}
